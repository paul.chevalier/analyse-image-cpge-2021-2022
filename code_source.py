import matplotlib.image as mpimg
import os as os
import numpy as np
import matplotlib.pyplot as plt
import copy as copy
from time import *
import cv2
import inspect

# Detection du chemin.
chemin = os.path.realpath(os.path.abspath
                          (os.path.split(inspect.getfile
                           (inspect.currentframe()))[0]))
os.chdir(chemin)

# Image analysée.
Image_analysee = np.array(mpimg.imread('Image_test.jpg'))
Image_analysee.setflags(write=1)

# Doit être la même que l'image analysée.
Image_affichee = cv2.imread('Image_test.jpg')
Image_affichee_copie = copy.deepcopy(Image_affichee)

# Permet d'agrandir notre image (car souvent petite).
Image_affichee_copie = cv2.resize(Image_affichee, (0, 0), fx=3, fy=3)


## FONCTION DE DEMONSTRATION
# Affiche l'image test colorée (vert repéré) et le nombre de pixels colorés.
# On fournit l'image à analyser ainsi que le pas d'analyse grossière.
# On le fixe à 10 car c'est un bon compromis entre rapidité et précision.
# L'analyse fine se fera sur les bords avec un pas de 1.

def test():
    """Fonction de demonstration a lancer sans argument"""
    Imagecoloree = detourage_precis_feuilles(Image_analysee, 10)


## COLORATION GROSSIERE DU VERT ET COMPTAGE
# Cette fonction permet le repérage et le coloriage du vert de notre image.

def coloriage_grossier_feuilles(Image, step, cadreanalyse=False):
    """Prend en entree l'image à analyser, le pas d'analyse
    (entier pair ou 1 ; correspond à l'espace entre deux pixels analyses)
    ainsi que les coordonnees de debut et fin d'analyse
    (sous forme debut_ligne, fin_ligne, debut_colonne, fin_colonne)
    si on ne veut pas analyser toute l'image, sinon on ne met rien"""

    Imagecoloree = copy.deepcopy(Image)
    Nbpixel = 0  # Initialisation du compteur de pixels colorés.

    # Vérification, fixation du pas, établissement éventuel du cadre d'analyse.

    if step/2 != int(step/2) and float(step/2) != 1/2:
        return("Le pas doit être pair")
    if cadreanalyse:
        debut_ligne, fin_ligne, debut_colonne, fin_colonne = cadreanalyse
        Matricebinaire = []
    else:
        debut_ligne, debut_colonne = 0, 0
        fin_ligne, fin_colonne, a = Image.shape

        # Matrice de même taille que l'image avec des 0 à la place des pixels.
        # Permettra de repérer les fins et debuts de feuille).

        Matricebinaire = np.zeros([Image.shape[0], Image.shape[1]])
    if step != 1:
        step, hstep = int(step), int(step/2)
    elif step == 1:  # Cas où le pas donné par l'utilisateur est 1.
        step, hstep = int(step), int(1)

    # Parcours de l'image ou du cadre et comptage du nombre de pixels verts.

    for ligne in range(debut_ligne+hstep, fin_ligne-hstep, step+1):
        for colonne in range(debut_colonne+hstep, fin_colonne-hstep, step+1):

            # Condition de sélection du pixel (voir fonction figure).

            if Imagecoloree[ligne][colonne][1] >= 30:
                if (Imagecoloree[ligne][colonne][0]**3 +
                        Imagecoloree[ligne][colonne][2]**3)*0.85 <= \
                            Imagecoloree[ligne][colonne][1]**3:
                    Nbpixel += 1

                    if not cadreanalyse:
                        # Ce "pixel" prend la valeur 1 dans la matrice.
                        Matricebinaire[ligne][colonne] = 1

                    # Coloriage du carré (côté=step+1) centré sur le pixel vert

                    for l2 in range(-hstep, hstep+1):
                        for c2 in range(-hstep, hstep+1):

                            Imagecoloree[ligne+l2][colonne+c2] = ([255, 0, 0])

                    # Permet de repérer où l'analyse a été realisée.

                    Imagecoloree[ligne][colonne] = ([0, 0, 255])

    # Calcul du nombre de pixels colorés.

    Nbpixel = Nbpixel*((step+1)**2)
    plt.imshow(Imagecoloree)
    plt.show()
    return Imagecoloree, Matricebinaire, step, hstep, Nbpixel


## AFFINAGE DU COLORIAGE ET COMPTAGE
# Affine l'analyse en repassant avec un pas plus fin sur les bords des feuilles
# Repère ces bords puis les décolore.
# Utilise coloriage_grossier_feuilles localement avec un pas plus fin.

def detourage_precis_feuilles(Image, step):
    """Prend en entree l'image à analyser et le pas pour l'analyse grossiere"""
    Original = copy.deepcopy(Image)  # Accès à l'image originale.
    ListeFrontiere = []

    # Acquisition préalable des données de coloriage_grossier_feuilles.

    Imagecoloree, Matricebinaire, step, hstep, Nbpixel = \
        coloriage_grossier_feuilles(Image, step)
    print('coloriage grossier realise')

    # Balayage de la Matricebinaire et repérage des débuts et fins de feuille.
    # Contiendra les coordonnees des pixels de debut (fins) de bord de feuille.

    RepriseDebut = []
    RepriseFin = []
    Reprise = [RepriseFin, RepriseDebut]
    # Contient les valeurs intéressantes de difference_Horizontal/Vertical.
    Liste_Valeurs_difference = [1, -1]

    for ligne in range(hstep, Matricebinaire.shape[0]-(step+1)):
        for colonne in range(hstep, Matricebinaire.shape[1]-(step+1)):

            # On nomme les soustractions suivant le sens.

            difference_Horizontal, difference_Vertical = \
                Matricebinaire[ligne][colonne] - \
                Matricebinaire[ligne][colonne+step+1],\
                Matricebinaire[ligne][colonne] - \
                Matricebinaire[ligne+step+1][colonne]

            # Si présence de fin (1-0 = 1) ou de début de feuille (0-1 = -1).
            # Ajout du pixel vert à la liste appropriée.

            for i in range(len(Liste_Valeurs_difference)):
                if difference_Horizontal == Liste_Valeurs_difference[i]:
                    Reprise[i].append([ligne,
                                      colonne + (i * step) + i,
                                      "Horizontal"])
                if difference_Vertical == Liste_Valeurs_difference[i]:
                    Reprise[i].append([ligne + (i * step) + i,
                                      colonne,
                                      "Vertical"])

    # Décoloration des pixels situés à l'extérieur de la feuille.

    Imagecoloree, Nbpixel = \
        decoloration(Imagecoloree, Original, Reprise, Nbpixel, hstep)
    print('decoloration realisee')

    # Repérage précis de la frontière de la feuille.

    ListeFrontiere = Frontiere(Original, Reprise)
    print('Frontiere realisee')

    # Recoloration fine et comptage final.

    Imagecoloree, Nbpixel = \
        recoloration(Imagecoloree, ListeFrontiere, Nbpixel, hstep)

    plt.imshow(Imagecoloree)
    plt.show()
    return Nbpixel


# Retire le rouge situé vers l'extérieur sur les bords de la feuille.
# C'est-à-dire décolore un rectangle de hstep de large et step de long.

def decoloration(Imagecoloree, Original, Reprise, Nbpixel, hstep):
    """Prend en argument l'image colorée par coloriage_grossier_feuilles,
    l'image originale, la liste Reprise,
    le nombre de pixels calculé par coloriage_grossier_feuilles et hstep"""

    # Parcours des listes RepriseFin (i=0) et RepriseDebut (i=1).

    for i in range(len(Reprise)):
        for j in range(len(Reprise[i])):

            # Correspond aux côtés du rectangle.

            for grand_cote in range(-hstep, hstep+1):
                for petit_cote in range((-i*hstep)+1-i, (1-i)*hstep+1-i):

                    # Le rectangle est ici "debout" sur son coté le plus court.

                    if Reprise[i][j][2] == "Horizontal":

                        # Évite le cas des "coins" (pixel déjà décoloré).

                        if (Imagecoloree[Reprise[i][j][0]+grand_cote]
                                [Reprise[i][j][1]+petit_cote][0] == 255):

                            # Le pixel désigné reprend sa couleur originale.

                            (Imagecoloree[Reprise[i][j][0]+grand_cote]
                                [Reprise[i][j][1]+petit_cote]) = \
                                (Original[Reprise[i][j][0]+grand_cote]
                                    [Reprise[i][j][1]+petit_cote])

                            # Pixel retiré du compte des pixels verts.
                            # Sera potentiellement réanalysé par la suite.

                            Nbpixel = Nbpixel-1

                    # Cas vertical (rectangle allongé sur son coté long).

                    else:
                        if (Imagecoloree[Reprise[i][j][0]+petit_cote]
                                [Reprise[i][j][1]+grand_cote][0] == 255):

                            # On inverse les positions des côtés.

                            (Imagecoloree[Reprise[i][j][0]+petit_cote]
                                [Reprise[i][j][1]+grand_cote]) = \
                                (Original[Reprise[i][j][0]+petit_cote]
                                    [Reprise[i][j][1]+grand_cote])

                            Nbpixel = Nbpixel-1
    return Imagecoloree, Nbpixel


# Recolore plus finement les débuts de feuilles.
# On sait précisement où commence la feuille grâce à Frontiere.

def recoloration(Imagecoloree, Frontiere, Nbpixel, hstep):
    """Prend en entrée l'image décolorée,
    la liste fourniepar la fonction Frontière,
    le nombre de pixel après décoloration ainsi que hstep"""

    # Parcours des listes FrontiereDebut et FrontiereFin.

    for i in range(len(Frontiere)):
        for j in range(len(Frontiere[i])):

            # Cas du parcours vertical.

            if Frontiere[i][j][2] == "Vertical":

                # Recoloration avec pas de 1.
                # Analyse du début exact de la feuille,
                # jusqu'au pixel de référence pour la fonction Frontiere.

                Imagecoloree, O2, step2, hstep2, Nbpixel2 = \
                    coloriage_grossier_feuilles(Imagecoloree, 1,
                                                ((Frontiere[i][j][0] -
                                                    i * Frontiere[i][j][3]),
                                                    (Frontiere[i][j][0] +
                                                        (1 - i) *
                                                        Frontiere[i][j][3] +
                                                        1),
                                                    (Frontiere[i][j][1] -
                                                        hstep),
                                                    (Frontiere[i][j][1] +
                                                        hstep + 1)))

            # Cas du parcours horizontal.

            else:
                Imagecoloree, O2, step2, hstep2, Nbpixel2 = \
                    coloriage_grossier_feuilles(Imagecoloree, 1,
                                                ((Frontiere[i][j][0] - hstep),
                                                    (Frontiere[i][j][0] +
                                                        hstep + 1),
                                                    (Frontiere[i][j][1] - i *
                                                        Frontiere[i][j][3]),
                                                    (Frontiere[i][j][1] +
                                                        (1 - i) *
                                                        Frontiere[i][j][3] +
                                                        1)))

            # Ajout des pixels colorés en rouge au compte final.

            Nbpixel = Nbpixel+Nbpixel2
    return Imagecoloree, Nbpixel


## REPERAGE DE LA FRONTIERE
# Utilise les listes de la fonction detourage_precis_feuilles.
# Repère le dernier/premier pixel vert de la feuille.
# Ce pixel est compris entre un pixel vert et un pixel non vert,
# séparés d'une distance de step pixels non analysés.

def Frontiere(Original, Reprise):
    """On prend l'image originale en entree ainsi que les listes
    produites par la fonction detourage_precis_feuilles donnant les coordonnées
    des pixels verts de fin et debut de feuille
    (ceux-ci n'etant pas nécessairement les derniers de la feuille !)"""

    # Contiendra les coordonnées des pixels de frontière de bord de feuille.

    FrontiereDebut = []
    FrontiereFin = []
    ListeFrontiere = [FrontiereDebut, FrontiereFin]

    # Parcours des listes RepriseFin (i=0) et RepriseDebut (i=1).

    for i in range(len(Reprise)):
        for j in range(len(Reprise[i])):

            # Distance entre pixel de départ et le dernier pixel vert trouvé.

            Distance = 0

            # Coordonnées du pixel de départ d'analyse.

            ligne, colonne = Reprise[i][j][0], Reprise[i][j][1]

            # Cas du parcours vertical.

            if Reprise[i][j][2] == "Vertical":

                # Tant qu'on trouve un pixel vert (qu'on est dans la feuille).

                while (Original[ligne + 1 - 2 * i][colonne][0]**3 +
                        Original[ligne + 1 - 2 * i][colonne][2]**3) * 0.85 <=\
                        Original[ligne + 1 - 2 * i][colonne][1]**3:
                    if Original[ligne][colonne][1] >= 30:

                        # On prend le pixel juste au dessus en début de feuille
                        # Celui dessous en fin de feuille.

                        ligne = (1 - 2 * i) + ligne
                        Distance += 1

                # Ajout des informations à la liste correspondante.

                ListeFrontiere[1-i].append([ligne, colonne,
                                            "Vertical", Distance])

            # Cas du parcours horizontal.

            else:
                while (Original[ligne][colonne + 1 - 2 * i][0]**3 +
                        Original[ligne][colonne + 1 - 2 * i][2]**3) * 0.85 <=\
                        Original[ligne][colonne + 1 - 2 * i][1]**3:
                    if Original[ligne][colonne][1] >= 30:
                        colonne = (1 - 2 * i) + colonne
                        Distance += 1
                ListeFrontiere[1-i].append([ligne, colonne,
                                            "Horizontal", Distance])
    return ListeFrontiere


## FONCTION D'ANALYSE

def Analyse():
    """C'est cette fonction qu'il faut lancer (sans argument)
    pour analyser l'image"""
    Image_affichee_copie = copy.deepcopy(Image_affichee)
    Image_affichee_copie = cv2.resize(Image_affichee, (0, 0), fx=3, fy=3)
    cv2.imshow('Affichage', Image_affichee_copie)
    cv2.setMouseCallback('Affichage', Clics)  # Appel à la fonction Clics.


## REPERAGE DES CLICS
# Permet de placer deux points sur notre image et ainsi d'indiquer l'échelle.

Listecoord = []  # Contiendra les coordonnees des pixels


def Clics(event, x, y, flags, params):
    """Cette fonction est appellée successivement par le module cv2
    en cas de déplacement de la souris,
    les arguments sont fournis automatiquement"""
    if len(Listecoord) != 2:
        if event == cv2.EVENT_LBUTTONDOWN:
            Listecoord.append([x, y])
            cv2.circle(Image_affichee_copie, (x, y),
                       10, (255, 0, 0), cv2.FILLED)
            cv2.imshow('Affichage', Image_affichee_copie)
    elif len(Listecoord) == 2:
        cv2.destroyWindow('Affichage')
        Lancement(Listecoord, Demande())


## FONCTION D'ECHELLE ET DE LANCEMENT

def Demande():  # Demande de saisir la taille réelle de l'échelle
    """Demande à l'utilisateur de saisir la taille réelle de l'échelle"""
    Taille = input('Saisir la distance en cm')
    return Taille


def Lancement(Liste, Taille):  # Donne la surface verte de l'image (en cm**2)
    """Prend en entrée la liste contenant les coordonnées
    des pixels de l'échelle (fournie par Clics)
    ainsi que leur distance en centimètre"""

    # Contiendra les coordonnées des deux pixels de début et fin de l'échelle.

    Listecoord = []

    # Distance "en pixel" entre les 2 points (Pythagore).

    Distancepixel = \
        np.sqrt(((Liste[0][0] / 3 - Liste[1][0] / 3)**2) +
                ((Liste[0][1] / 3 - Liste[1][1] / 3)**2))

    # Récupération du nombre de pixels colorés total.

    Nbpixel = detourage_precis_feuilles(Image_analysee, 10)
    Taille_un_pixel = (float(Taille)/Distancepixel)
    Surface = (Taille_un_pixel**2)*Nbpixel
    print("Surface =", Surface, "cm**2")


## FIGURE DES COULEURS PAR NIVEAU DE VERT : JUSTIFICATION DU COEFFICIENT
# Permet de justifier le coefficient de sélection du vert,
# utilisé dans coloriage_grossier_feuilles,
# en affichant les couleurs associées pour une valeur de vert fixée.

def figure(Valeur_de_vert, Coeff):  # Coefficient retenu : 0.85
    """Prend en entrée une valeur de vert [0;255] ainsi qu'un coefficient"""
    Matrice = []
    Ligne = []
    for Rouge in range(0, 256):
        for Bleu in range(0, 256):
            if (Rouge**(3)+Bleu**(3))*Coeff <= (Valeur_de_vert)**(3):
                Pixel = [Rouge, Valeur_de_vert, Bleu]
                Ligne.append(Pixel)
            else:
                Pixel = [0, 0, 0]
                Ligne.append(Pixel)
        Matrice.append(Ligne)
        Ligne = []
    plt.imshow(Matrice)
    plt.show()
